import sbt._
import Keys._

object Build extends Build {

  override lazy val settings = super.settings ++ Seq(
    libraryDependencies ++= Seq(
      "org.apache.kafka" %% "kafka" % "0.8.1.1" excludeAll(
        ExclusionRule(organization = "javax.jms"),
        ExclusionRule(organization = "com.sun.jdmk"),
        ExclusionRule(organization = "com.sun.jmx")
        ),
      "com.typesafe.akka" %% "akka-actor" % "2.3.3",
      "org.slf4j" % "slf4j-log4j12" % "1.7.5",

      "org.scalatest" %% "scalatest" % "2.2.0" % "test"
    ),
    scalacOptions ++= Seq("-unchecked", "-deprecation", "-Xlint"),
    organization := "me.lazyval",
    scalaVersion := "2.10.4",
    initialCommands in console := "import me.lazyval._"
  )

  lazy val root = Project(id = "fixing-consumer-code", base = file("."), settings = Project.defaultSettings ++ settings) dependsOn kafkaExtras

  lazy val kafkaExtras = RootProject(uri("https://omnomnom@bitbucket.org/omnomnom/kafka-extras.git#v0.1.0-lw"))
}