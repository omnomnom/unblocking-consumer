import java.util.Properties
import kafka.consumer.Consumer
import kafka.consumer.ConsumerConfig
import kafka.consumer.Whitelist
import kafka.serializer.DefaultDecoder

case class KafkaConsumer(topic: String, groupId: String, zookeeperConnect: String, readFromStartOfStream: Boolean = true) {
 
  val props = new Properties()
//  props.put("codec", "json")
  props.put("group.id", groupId)
  props.put("zookeeper.connect", zookeeperConnect)
  props.put("consumer.timeout.ms", "500")
  props.put("auto.offset.reset", if (readFromStartOfStream) "smallest" else "largest")
 
  val config = new ConsumerConfig(props)
  val connector = Consumer.create(config)
  val filterSpec = new Whitelist(topic)
  val stream = connector.createMessageStreamsByFilter(filterSpec, 1, new DefaultDecoder(), new DefaultDecoder())
 
  // very basic reading
  def read = {
    for (messageAndTopic <- stream) {
      var aux = messageAndTopic.iterator
 
      while (aux.hasNext) {
        var msg = aux.next.message
        println(new String(msg))
      }
    }
  }
 
  // 1st try
  // inspiration taken from ConsoleConsumer @ Kafka Git repo (v0.8.0 branch)
  def batchRead(maxMessages: Int = 100, skipMessageOnError: Boolean = true): Map[Array[Byte], Array[Byte]] = {
    var numMessages = 0L
    //    val formatter: MessageFormatter = messageFormatterClass.newInstance().asInstanceOf[MessageFormatter]
    //    formatter.init(formatterArgs)
    var map = Map[Array[Byte], Array[Byte]]()
    try {
      //      val stream = connector.createMessageStreamsByFilter(filterSpec, 1, new DefaultDecoder(), new DefaultDecoder()).get(0)
 
      val iter = if (maxMessages >= 0)
        stream.slice(0, maxMessages)
      else
        stream
 
      for (messageAndTopic <- iter) {
        for (m <- messageAndTopic) {
          try {
            // formatter.writeTo(messageAndTopic.key, messageAndTopic.message, System.out)
            println(m.key + " --- " + new String(m.message))
            map + (m.key -> m.message)
            numMessages += 1
          } catch {
            case e: Throwable =>
              if (skipMessageOnError)
                // error("Error processing message, skipping this message: ", e)
                println("Error processing message, skipping this message: ", e)
              else
                throw e
          }
          if (System.out.checkError()) {
            // This means no one is listening to our output stream any more, time to shutdown
            System.err.println("Unable to write to standard out, closing consumer.")
            System.err.println("Consumed %d messages".format(numMessages))
            // formatter.close()
            connector.shutdown()
            System.exit(1)
          }
        }
      }
    } catch {
      case e: Throwable =>
        // error("Error processing message, stopping consumer: ", e)
        println("Error processing message, stopping consumer: ", e)
    }
    System.err.println("Consumed %d messages".format(numMessages))
    System.out.flush()
    // formatter.close()
    connector.shutdown()
    map
  }
 
  def close() {
    connector.shutdown()
  }
 
  // 2nd try
  def tryBatchRead2(maxMessages: Int = 100, skipMessageOnError: Boolean = true): List[String] = { // Map[Long, Array[Byte]] = {
    var numMessages = 0L
 
    // var map = Map[Long, Array[Byte]]()
    var list = List[String]()
 
    val iter = if (maxMessages >= 0) stream.slice(0, maxMessages) else stream
 
    for (messageAndTopic <- iter) {
      for (m <- messageAndTopic) {
        println(m.offset.toString + " --- " + new String(m.message))
        list = list ++ List(new String(m.message))
        println("DEBUG " + list)
        numMessages += 1
      }
      println("test1")
    }
 
    println("test2")
    println("FINISH" + list)
    // System.out.flush()
    connector.shutdown()
    println("test3")
    list
  }
 
  // 3rd try
  def tryBatchRead3(maxMessages: Int = 100): Seq[String] = {
    val batchStream = stream.take(maxMessages)

    try {
      for {
        messageAndTopic <- batchStream.take(maxMessages)
        msg <- messageAndTopic.take(maxMessages).toList
      } yield {
        val m = new String(msg.message())
        println(msg.offset.toString + " --- " + m)
        m
      }
    } catch {
      case ex: Exception => Nil // TODO: make an appropriate return
    }
  }
 
}