import java.io.File
import org.scalatest._
import org.scalatest.concurrent.{TimeLimitedTests, ScalaFutures}
import org.scalatest.time.SpanSugar._
import scala.collection.JavaConversions.asJavaCollection
import scala.collection.JavaConversions.mapAsJavaMap

import me.lazyval.kafkaextras._
import Utils.TMP_DIR

class KafkaTest extends FlatSpec with Matchers with ScalaFutures with BeforeAndAfter with TimeLimitedTests {
  val timeLimit = 1 second
  val msgs = List("foo", "bar", "baz")

  var zk: EmbedZookeeper = _
  var kafka: EmbedKafka = _
  var consumer: KafkaConsumer = _

  before {
    val zkPort = Utils.findRandomPort()
    // TODO: cleanup zk directories as well
    zk = new EmbedZookeeper(zkPort, new File(TMP_DIR, "zk@" + zkPort))
    zk.start()

    val builder = Brokers.kafkaBuilder()
      .withRandomPort()
      .withZkConnection("localhost:" + zkPort)
      .placeDataInTmpDir()

    kafka = builder.build()
    kafka.start()

    Producers.send(msgs, "test-topic", "localhost:" + builder.port)

    consumer = new KafkaConsumer("page-visits", "test-consumer-group", "localhost:" + zkPort, readFromStartOfStream = true)
  }

  after {
    kafka.stop()
    Thread.sleep(1000) // take some time to tear down
    zk.stop()
  }

  "First implementation " should "not block on reading" in {
    val list = consumer.batchRead(2)
    list.foreach(println)
  }

  "Second implementation " should "not block on reading" in {
    val list = consumer.tryBatchRead2(2)
    list.foreach(println)
  }

  "Third implementation" should "not block on reading" in {
    val list = consumer.tryBatchRead3(2)
    list.foreach(println)
  }
}